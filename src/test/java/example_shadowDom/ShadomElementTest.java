package example_shadowDom;


import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;


import java.io.*;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class ShadomElementTest {

  private static WebDriver webdriver;
  Properties property = new Properties();

    @BeforeTest
  public void invoke_chrome(){
        System.setProperty(getPropertyof("drivertypename_chrome"),getPropertyof("driverpath_chrome"));
        ChromeOptions options = new ChromeOptions();
        //options.setHeadless(true);
        webdriver = new ChromeDriver(options);
        webdriver.get(getPropertyof("url"));
        webdriver.manage().timeouts().implicitlyWait(Long.parseLong(property.getProperty("time.seconds")), TimeUnit.SECONDS);
         operationonWebpage();

    }
   @AfterTest
   public void  quitDriver(){
       webdriver.quit();
   }

   public WebElement getShadowElement(WebElement element){
       WebElement shadowelement = (WebElement) ((JavascriptExecutor) webdriver)
               .executeScript("return arguments[0].shadowRoot",element);
       return shadowelement;
   }

   @Test
   public void operationonWebpage(){

         WebElement firstElement = webdriver.findElement(By.tagName("downloads-manager"));
         WebElement firstShadow = getShadowElement(firstElement);

       WebElement secondElement = firstShadow.findElement(By.cssSelector("downloads-toolbar"));
       WebElement secondShadow = getShadowElement(secondElement);

       WebElement thirdElement = secondShadow.findElement(By.cssSelector("cr-toolbar"));
       WebElement thirdShadow = getShadowElement(thirdElement);


       Assert.assertEquals(thirdShadow.findElement(By.cssSelector("#leftSpacer > h1")).getText(),"Downloads");

   }





  public String getPropertyof(String prop)
  {
                      try {

                          File file = new File(
                                  getClass().getClassLoader().getResource("settings.properties").getFile()
                          );
                          InputStream inputStream = new FileInputStream(file);
                          property.load(inputStream);

                      } catch (FileNotFoundException e) {
                          e.printStackTrace();
                      } catch (IOException e) {
                          e.printStackTrace();
                      }

                      return property.getProperty(prop);
  }

}//class
